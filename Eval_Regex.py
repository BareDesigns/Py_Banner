from docx import Document
import re
import pyperclip
import os
import time

# Reads the clipboard
transcript = str(pyperclip.paste())

# Sets the output directory
os.chdir('H:\\PYTHON_EVALUATIONS')


# Removes unnecessary text from the course lines
def course_clean(txt):

    '''This function cleans the COURSE lines to make them compatible with the
    AHK script'''
    txt = re.sub(r' +', ' ', txt)
    txt = re.sub(r'(\d)(.00)(\s*)$', 'IP', txt)
    txt = re.sub(r'(\d)$', ' ', txt)
    txt = re.sub(r'((\W)|(\W\s*))$', '', txt)
    txt = re.sub(r'(\s*E)$', '', txt)
    txt = re.sub(r'(\s*E\s*\d*\s*)$', '', txt)
    txt = re.sub(r'([Z]+)$', 'IP', txt)
    txt = re.sub(r'(\s*[I]\s*)$', '', txt)
    return txt


def terms(txt):

    # Rearranges the year/semester order for the AHK script to work
    if bool(re.search(r'^(\s*)(<<)(\d{4})', txt)) is True:
        term = re.compile(r'(\d{4})\s(Summer|Spring|Fall)')
        mo = term.search(txt)
        mo.groups()
        year, Semester = mo.groups()
        new_term = re.sub(r'(\s*)(<<)(\d{4})(\s)(Fall|Spring|Summer):', '<<' +
                          Semester + ' ' +
                          year + ' :', txt)
        return new_term

    # Removes the word "Credit" from the term line
    elif re.match(r'^(<<Credit)\s', txt) is True:
        txt = re.sub(r'^(<<Credit)\s', '<<', txt)
        return txt

    # Removes the word "SEMESTER" from the term line
    elif bool(re.search(r'^\s*<<(FALL|SUMMER|SPRING)\s(SEMESTER)',
                        txt)) is True:
        term = re.compile(r'^\s*<<(FALL|SPRING|SUMMER)\s(SEMESTER)\s(\d{4})')
        foo = term.search(txt)
        foo.groups()
        Term, Semester, Year = foo.groups()
        new_term = re.sub(r'''
                          ^(\s*)(<<)(FALL|SUMMER|SPRING)
                          (\s)(SEMESTER)(\s)(\d{4}):''',
                          '<<' + Term + ' ' + Year + ' :', txt)
        return new_term

    # Adds a space after the year
    elif bool(re.search(r'^\s*<<(Spring|Summer|Fall)\s(\d{4}:)', txt)) is True:
        term = re.compile(r'\s*<<(Spring|Summer|Fall)\s(\d{4})')
        hoo = term.search(txt)
        hoo.groups()
        Term, Year = hoo.groups()
        new_term = re.sub(r'^\s*<<(Spring|Summer|Fall)\s(\d{4}):',
                          '<<' + Term + ' ' + Year + ' :', txt)
        return new_term

    # Removes the I/II after Summer Term
    elif bool(re.search(r'^\s*<<(Summer)\s(I|II)', txt)) is True:
        term = re.compile(r'^\s*<<(Summer)\s(I|II)\s(\d{4})',
                          re.IGNORECASE)
        loo = term.search(txt)
        loo.groups()
        Term, Summer_Term, Year = loo.groups()
        new_term = re.sub(r'^\s*<<(Summer|SUMMER)\s(I|II)\s(\d{4})\s:',
                          '<<' + Term + ' ' + Year + ' :', txt)
        return new_term

    else:
        return txt


def name_and_college(txt):

    # Cleans the college/university/student name line
    txt = re.sub(r'^(\s+)', '', txt)           # Removes multiple blanks
    txt = re.sub(r'^(\s*)(AS:)', '', txt)
    return txt


# The file name will be set to the current date
file_time = time.strftime('%m' + '-' + '%d' + '-' + '%Y' + '.docx')
document = Document()

for line in transcript.splitlines(False):

    # Writes the G# line down in BOLD
    if re.match(r'^(G00|G\sG)', line):
        p = document.add_paragraph('=' * 50 + '\n')
        p.add_run('\n' + line).bold = True

    # Writes the FICE Code in BOLD
    elif re.match(r'^\d{6}', line):
        p.add_run('\n' + line + '\n').bold = True

    # Writes the term line
    elif re.match(r'^\s*<<', line):
        p.add_run('\n' + terms(line).lstrip() + '\n')

    # Writes the College name
    elif re.match(r'^\s*AS:\s', line):
        p.add_run('\n' + name_and_college(line).lstrip()).bold = True

    # Writes the Student name
    elif re.match(r'^\s*Student\sName', line):
        p.add_run('\n' + name_and_college(line) + '\n').bold = True

    # Writes the course line
    elif re.match(r'^\s*[A-Z]{1,7}\s+\d{1,5}', line):
        p.add_run(course_clean(line).lstrip() + '\n')

    # This is specifically for those weird course with spaces in name
    elif re.match(r'^\s*[A-Z]{1,7}\s[A-Z]{1,7}\s*\d{1,5}', line):
        p.add_run(course_clean(line).lstrip() + '\n')

    # Converts transfer line to a pretter text and writes to file
    elif re.match(r'^\s*\*\sInst', line):
        p.add_run('!!!!!!!TRANSFER HOURS!!!!!!!!\n').italic = True

    # TODO: Add line for Coastline Comunity College Weirdness

    else:
        continue

# document.save(file_name + '.docx')
document.save(file_time)
